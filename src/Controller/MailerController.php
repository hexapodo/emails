<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;





class MailerController extends AbstractController
{
    /**
     * @Route("/email", methods={"POST"})
     */
    public function sendEmail(MailerInterface $mailer, Request $request): JsonResponse
    {
        $email = (new Email())
            ->from('hexapodo@gmail.com')
            ->to('info@kishron.com.co')
            ->subject('Mensaje desde la página de Kishron')
            ->text('Sending emails is fun again!');

        $cont = json_decode($request->getContent(), true);

        $html = "<br>< < < MENSAJE RECIBIDO DESDE KISHRON.COM.CO > > ><br><br><br>";
        $html .= "Nombre: <strong>"
            . $cont['name'] . "</strong><br><br>";
        $html .= "Email: <strong>"
            . $cont['email'] . "</strong><br><br>";
        $html .= "Asunto: <strong>"
            . $cont['subject'] . "</strong><br><br>";
        $html .= "Mensaje:<br><strong>"
            . $cont['message'] . "</strong><br><br><br><br>";

        $email->html($html);
        $result = $mailer->send($email);

        return new JsonResponse(array(
            'success'=> true,
        ));
    }
}